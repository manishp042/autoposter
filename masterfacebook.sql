/* Database export results for db masterfacebook */

/* Preserve session variables */
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS;
SET FOREIGN_KEY_CHECKS=0;

/* Export data */

/* Table structure for migrations */
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/* data for Table migrations */
INSERT INTO `migrations` VALUES (1,"2014_10_12_000000_create_users_table",1);
INSERT INTO `migrations` VALUES (2,"2014_10_12_100000_create_password_resets_table",1);
INSERT INTO `migrations` VALUES (3,"2018_09_17_182155_add_new_column_to_users_table",1);

/* Table structure for password_resets */
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/* data for Table password_resets */

/* Table structure for users */
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `facebook_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/* data for Table users */
INSERT INTO `users` VALUES (1,"kkk","EE@mail.com",NULL,"$2y$10$Kflv4ygaxrZrT9kNkgXUd.tQ/W.b2FukuOMNIlrSu3hbjCYHlmRN.","d5Ohbzr7AOdcNti8KOpTU5r1UVlMGtUMTbYw41i6bpsVZv96vynxATKx7m0V","2018-09-17 23:31:26","2018-09-17 23:31:26",NULL,NULL,NULL);
INSERT INTO `users` VALUES (6,"Manish Pandey","Manishp042@gmail.com",NULL,"$2y$10$iZkT5lnvBhtUxzsit/NM..EqKq38LaYI9nXL7K.iyqnLs6eSf4DDe","j4PUnQBYkq7JD4d4nY6G0gnMC04eWFxzc0vZW9DIIXpcP09vzyZ48CAEg0di","2018-09-30 14:30:36","2018-09-30 14:30:36",NULL,NULL,NULL);

/* Restore session variables to original values */
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
