@extends('layouts.master')

@section('header')
<link href="{{asset('js/summernote/summernote.css')}}" rel="stylesheet">
<link rel="stylesheet" hrerf="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css">
<!-- DataTables -->
<link href="{{asset('plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Responsive datatable examples -->
<link href="{{asset('plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('navbar')
<!-- Start content -->
<style>
body {font-family: Arial;}

/* Style the tab */
.tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}
</style>
<div class="content">
    <div class="container-fluid">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"><i class="ti-file"></i> <span>Add New</span></h4>
                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <!-- <li class="breadcrumb-item"><a href="#"></a></li> -->
                        <li class="breadcrumb-item active">Facebook Account</li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    @endsection
    @section('content')
        <div class="row">
            <div class="col-sm-7">
                <div class="card-box">
                <div class="tab">
                   <button class="tablinks" onclick="openCity(event, 'text')">Text</button>
                   <button class="tablinks" onclick="openCity(event, 'link')">Link</button>
                   <button class="tablinks" onclick="openCity(event, 'image')">Image</button>
                   <button class="tablinks" onclick="openCity(event, 'video')">video</button>
                   <button class="tablinks" onclick="openCity(event, 'multiple_image')">Multiple Image</button>
                </div>
                
                <div id="text" class="tabcontent">
                        <form method="post" action="{{ route('text') }}">
                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                        <div class="form-group">
                            <input type="text" name="title" placeholder="Enter the Title" class="form-control" />
                        </div>
                        <div class="form-group">
                            <textarea id="content" name="content" class="form-control" col="30" rows="10"></textarea>
                        <!-- <textarea id="txtEditor" name="area"></textarea> -->
                       </div>
                       <input type="submit" value="submit" />
                    </form>
                </div>

                    <div id="link" class="tabcontent">
                    <form method="post" action="{{ route('text') }}">
                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                    <div class="form-group">
                    <input type="text" name="title" placeholder="Enter the Link Title" class="form-control" />
                    </div>
                    <div class="form-group">
                    <textarea id="content" name="content" class="form-control" col="30" rows="10"></textarea>
                    <!-- <textarea id="txtEditor" name="area"></textarea> -->
                    </div>
                    <input type="submit" value="submit" />
                    </form>
                    </div>

                    <div id="image" class="tabcontent">
                    <form method="post" action="{{ route('text') }}">
                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                    <div class="form-group">
                    <input type="text" name="title" placeholder="Enter the Image Title" class="form-control" />
                    </div>
                    <div class="form-group">
                    <textarea id="content" name="content" class="form-control" col="30" rows="10"></textarea>
                    <!-- <textarea id="txtEditor" name="area"></textarea> -->
                    </div>
                    <input type="submit" value="submit" />
                    </form>
                    </div>

                    <div id="video" class="tabcontent">
                    <form method="post" action="{{ route('text') }}">
                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                    <div class="form-group">
                    <input type="text" name="title" placeholder="Enter the Video Title" class="form-control" />
                    </div>
                    <div class="form-group">
                    <textarea id="content" name="content" class="form-control" col="30" rows="10"></textarea>
                    <!-- <textarea id="txtEditor" name="area"></textarea> -->
                    </div>
                    <input type="submit" value="submit" />
                    </form>
                    </div>

                    <div id="multiple_image" class="tabcontent">
                    <form method="post" action="{{ route('text') }}">
                    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                    <div class="form-group">
                    <input type="text" name="title" placeholder="Enter the Multiple Image Title" class="form-control" />
                    </div>
                    <div class="form-group">
                    <textarea id="content" name="content" class="form-control" col="30" rows="10"></textarea>
                    <!-- <textarea id="txtEditor" name="area"></textarea> -->
                    </div>
                    <input type="submit" value="submit" />
                    </form>
                    </div>
                    
                </div>
            </div>
            <div class="col-sm-5">
                <div class="card-box">
                    <form method="post">
                        <div class="form-group">
                            <input type="text" placeholder="Enter the Title" class="form-control" />
                        </div>
                        <div class="form-group">
                            
                            <textarea id="elm1" name="area" class="form-control" col="30" rows="10"></textarea>
                       </div>
                    </form>
                
                    <div class="col-12">
                        <select class="form-control">
                            <option selected>Please select your appId</option>
                            <option>hsdfkfwierp645896567567578568678</option>
                            <option>hfhghjghjk7687867856</option>
                            <option>fhgfhjfj67586786786</option>
                            <option>fjghjghjghjkyhukykyukiyuikty</option>
                            <option>rtyhrtyrtyrtyrtyr56uyyth</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <!-- End row -->

        <div class="row">
        <div class="col-12">
            <div class="card-box table-responsive">
                <h4 class="m-t-0 header-title">Select/Pages/Groups/Profiles</h4>
                <p class="text-muted font-14 m-b-30"></p>
                <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Account</th>
                            <th>Type</th>
                            <th>Pravacy</th>
                            <th>Link</th>
                            <th>Process</th>
                        </tr>
                    </thead>

                     <div class="form-inline m-b-20">
                        <div class="row">
                            <div class="col-md-3 text-xs-center">
                                <div class="form-group">
                                    <!-- <label class="control-label m-r-5">Status</label> -->
                                    <select id="demo-foo-filter-status" class="form-control input-sm">
                                        <option value="">All Accounts</option>
                                        <option value="active">Active</option>
                                        <option value="disabled">Disabled</option>
                                        <option value="suspended">Suspended</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3 text-center text-right">
                                <div class="form-group float-right">
                                    <select id="demo-foo-filter-status" class="form-control input-sm">
                                        <option value="">All Categories</option>
                                        <option value="active">Active</option>
                                        <option value="disabled">Disabled</option>
                                        <option value="suspended">Suspended</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2  text-center text-right">
                                <div class="form-group float-right">
                                    <input type="checkbox" name="" value="" style="width:20px;height:20px"/><span>AllProfiles</span>
                                </div>
                            </div>
                            <div class="col-md-2 text-center text-right">
                                <div class="form-group float-right">
                                    <input type="checkbox" name="" value="" style="width:20px;height:20px"/><span>AllGroups</span>
                                </div>
                            </div>
                            <div class="col-md-2 text-center text-right">
                                <div class="form-group float-right">
                                    <input type="checkbox" name="" value="" style="width:20px;height:20px" /><span>All Pages</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <tbody>
                        <tr>
                            <!-- <td><input  type="checkbox"/><td> -->
                            <td>Tiger Nixon</td>
                            <td>System Architect</td>
                            <td>Edinburgh</td>
                            <td>61</td>
                            <td>2011/04/25</td>
                            <td>$320,800</td>
                        </tr>
                        <tr>
                            <!-- <td><input  type="checkbox"/><td> -->
                            <td>Garrett Winters</td>
                            <td>Accountant</td>
                            <td>Tokyo</td>
                            <td>63</td>
                            <td>2011/07/25</td>
                            <td>$170,750</td>
                        </tr>
                        <tr>
                            <!-- <td><input  type="checkbox"/><td> -->
                            <td>Ashton Cox</td>
                            <td>Junior Technical Author</td>
                            <td>San Francisco</td>
                            <td>66</td>
                            <td>2009/01/12</td>
                            <td>$86,000</td>
                        </tr>
                        
                        <tr>
                            <!-- <td><input  type="checkbox"/><td> -->
                            <td>Colleen Hurst</td>
                            <td>Javascript Developer</td>
                            <td>San Francisco</td>
                            <td>39</td>
                            <td>2009/09/15</td>
                            <td>$205,500</td>
                        </tr>
                        <tr>
                            <!-- <td><input  type="checkbox"/><td> -->
                            <td>Sonya Frost</td>
                            <td>Software Engineer</td>
                            <td>Edinburgh</td>
                            <td>23</td>
                            <td>2008/12/13</td>
                            <td>$103,600</td>
                        </tr>
                        <tr>
                            <!-- <td><input  type="checkbox"/><td> -->
                            <td>Jena Gaines</td>
                            <td>Office Manager</td>
                            <td>London</td>
                            <td>30</td>
                            <td>2008/12/19</td>
                            <td>$90,560</td>
                        </tr>
                        <tr>
                            <!-- <td><input  type="checkbox"/><td> -->
                            <td>Quinn Flynn</td>
                            <td>Support Lead</td>
                            <td>Edinburgh</td>
                            <td>22</td>
                            <td>2013/03/03</td>
                            <td>$342,000</td>
                        </tr>
                        
                        <tr>
                            <!-- <td><input  type="checkbox"/><td> -->
                            <td>Yuri Berry</td>
                            <td>Chief Marketing Officer (CMO)</td>
                            <td>New York</td>
                            <td>40</td>
                            <td>2009/06/25</td>
                            <td>$675,000</td>
                        </tr>
                        <tr>
                            <!-- <td><input  type="checkbox"/><td> -->
                            <td>Caesar Vance</td>
                            <td>Pre-Sales Support</td>
                            <td>New York</td>
                            <td>21</td>
                            <td>2011/12/12</td>
                            <td>$106,450</td>
                        </tr>
                        <tr>
                            <!-- <td><input  type="checkbox"/><td> -->
                            <td>Doris Wilder</td>
                            <td>Sales Assistant</td>
                            <td>Sidney</td>
                            <td>23</td>
                            <td>2010/09/20</td>
                            <td>$85,600</td>
                        </tr>
                        <tr>
                            <!-- <td><input  type="checkbox"/><td> -->
                            <td>Angelica Ramos</td>
                            <td>Chief Executive Officer (CEO)</td>
                            <td>London</td>
                            <td>47</td>
                            <td>2009/10/09</td>
                            <td>$1,200,000</td>
                        </tr>
                        <tr>
                            <!-- <td><input  type="checkbox"/><td> -->
                            <td>Gavin Joyce</td>
                            <td>Developer</td>
                            <td>Edinburgh</td>
                            <td>42</td>
                            <td>2010/12/22</td>
                            <td>$92,575</td>
                        </tr>
                        <tr>
                            <!-- <td><input  type="checkbox"/><td> -->
                            <td>Jennifer Chang</td>
                            <td>Regional Director</td>
                            <td>Singapore</td>
                            <td>28</td>
                            <td>2010/11/14</td>
                            <td>$357,650</td>
                        </tr>
                        <tr>
                            <!-- <td><input  type="checkbox"/><td> -->
                            <td>Brenden Wagner</td>
                            <td>Software Engineer</td>
                            <td>San Francisco</td>
                            <td>28</td>
                            <td>2011/06/07</td>
                            <td>$206,850</td>
                        </tr>
                        <tr>
                            <!-- <td><input  type="checkbox"/><td> -->
                            <td>Michael Bruce</td>
                            <td>Javascript Developer</td>
                            <td>Singapore</td>
                            <td>29</td>
                            <td>2011/06/27</td>
                            <td>$183,000</td>
                        </tr>
                        <tr>
                            <!-- <td><input  type="checkbox"/><td> -->
                            <td>Donna Snider</td>
                            <td>Customer Support</td>
                            <td>New York</td>
                            <td>27</td>
                            <td>2011/01/25</td>
                            <td>$112,000</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endsection

@section('footerArea')
<!-- <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script> -->
<script src="{{asset('js/summernote/summernote.min.js')}}"></script>
<script>
    $(document).ready(function(){
        $('#content').summernote();
    });
</script>

<!-- Required datatable js -->
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<!-- Buttons examples -->
<script src="{{asset('plugins/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/datatables/jszip.min.js')}}"></script>
<script src="{{asset('plugins/datatables/pdfmake.min.js')}}"></script>
<script src="{{asset('plugins/datatables/vfs_fonts.js')}}"></script>
<script src="{{asset('plugins/datatables/buttons.html5.min.js')}}"></script>
<script src="{{asset('plugins/datatables/buttons.print.min.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        //Buttons examples
        var table = $('#datatable-buttons').DataTable({
            lengthChange: false,
            buttons: ['copy', 'excel', 'pdf']
        });
        table.buttons().container()
                .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
    } );
</script>
<script>
function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>


@endsection