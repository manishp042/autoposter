@extends('layouts.master')

@section('header')
<link rel="stylesheet" href="{{asset('plugins/magnific-popup/dist/magnific-popup.css')}}" />
<link rel="stylesheet" href="{{asset('plugins/jquery-datatables-editable/dataTables.bootstrap4.min.css')}}" />
<link href="{{asset('dark/assets/css/titatoggle-dist.css')}}" rel="stylesheet">
@endsection
@section('navbar')
<!-- Start content -->
<div class="content">
    <div class="container-fluid">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="m-t-0 header-title"><span>Google Maps</span></h4>
                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <!-- <li class="breadcrumb-item"><a href="#"></a></li> -->
                        <li class="breadcrumb-item active">Google Maps</li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    @endsection
    @section('content')
        <div class="row">
            <div class="col-lg-6">
                <div class="card-box">
                    <h4 class="m-t-0 m-b-20 header-title"><b>Markers</b></h4>

                    <div id="gmaps-markers" class="gmaps"></div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card-box">
                    <h4 class="m-t-0 m-b-20 header-title"><b>Overlays</b></h4>

                    <div id="gmaps-overlay" class="gmaps"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="card-box">
                    <h4 class="m-t-0 m-b-20 header-title"><b>Street View Panoramas</b></h4>

                    <div id="panorama" class="gmaps-panaroma"></div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card-box">
                    <h4 class="m-t-0 m-b-20 header-title"><b>Map Types</b></h4>

                    <div id="gmaps-types" class="gmaps"></div>
                </div>
            </div>
        </div>

    </div>
    <!-- end container -->
</div>
<!-- end content -->      
    @endsection

    @section('footerArea')
        <!-- google maps api -->
        <script src="http://maps.google.com/maps/api/js?key=AIzaSyDsucrEdmswqYrw0f6ej3bf4M4suDeRgNA"></script>

        <!-- Gmaps file -->
        <script src="{{asset('plugins/gmaps/gmaps.min.js')}}"></script>
        <!-- demo codes -->
        <script src="{{asset('dark/assets/pages/jquery.gmaps.js')}}"></script>

    @endsection
