@extends('layouts.master')

@section('header')
<link rel="stylesheet" href="{{asset('plugins/magnific-popup/dist/magnific-popup.css')}}" />
<link rel="stylesheet" href="{{asset('plugins/jquery-datatables-editable/dataTables.bootstrap4.min.css')}}" />
<link href="{{asset('dark/assets/css/titatoggle-dist.css')}}" rel="stylesheet">
@endsection
@section('navbar')
<!-- Start content -->
<div class="content">
    <div class="container-fluid">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h3 class="m-t-0 header-title"><i class="ti-facebook"></i> <span>Faceboook Account</span></h3>
                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <!-- <li class="breadcrumb-item"><a href="#"></a></li> -->
                        <li class="breadcrumb-item active">Facebook Account</li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    @endsection
    @section('content')
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-1 text-xs-center">
                            <div class="form-group">
                                <!-- <label class="control-label m-r-5">Status</label> -->
                                <select id="demo-foo-filter-status" class="form-control input-sm" style="background:#ec0b29 !important;color:white !important">
                                    <option value="active">Active</option>
                                    <option value="">Deactive</option>
                                    <option value="active">Delete</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="m-b-30">
                                <a href="{{route('facebookAccount.edit')}}" class="btn btn-success waves-effect waves-light">Add New Account <i class="mdi mdi-plus-circle-outline"></i></a>
                            </div>
                        </div>
                    </div><hr><br>
                    <table class="table table-striped add-edit-table" id="datatable-editable">
                        <thead>
                        <tr>
                            <th>Check</th>
                            <th>FaceBook ID</th>
                            <th>FullName</th>
                            <th>UserName</th>
                            <th>Token</th>
                            <th>UpdateGroup</th>
                            <th>UpdatePages</th>
                            <th>UpdateFriends</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="gradeX">
                            <td><input type="checkbox"  name="" value="" style="width:20px;height:20px;"/></td>
                            <td>1921181701295125</td>
                            <td>Policeman</td>
                            <td>georigy</td>
                            <td>kghfgk434556456dfw34ufs</td>
                            <td ><a href="" class="btn btn-primary btn-custom waves-effect w-md waves-light m-b-5">UpdateGroup<a></td>
                            <td ><a href="" class="btn btn-success btn-custom waves-effect w-md waves-light m-b-5">UpdatePages</td>
                            <td ><a href="" class="btn btn-info btn-custom waves-effect w-md waves-light m-b-5" 
                                style="background-color: #8b73d8; border: 1px solid #8b73d8;">UpdateFriends

                            </td>
                            <td>
                                <div class="form-check checkbox-slider-lg checkbox-slider--b-flat">
                                    <label>
                                        <input type="checkbox" checked=""><span></span>
                                    </label>
                                </div>
                            </td>
                            <td class="actions">
                                <a href="#" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                <a href="#" class="on-default remove-row" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
                                <a href="#" class="hidden on-editing save-row" data-toggle="tooltip" data-placement="top" title="" data-original-title="Save"><i class="fa fa-save"></i></a>
                                <a href="#" class="hidden on-editing cancel-row" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cancel"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        <tr class="gradeC">
                        <td><input type="checkbox"  name="" value="" style="width:20px;height:20px;"/></td>
                            <td>67619211817012951</td>
                            <td>kololiceman</td>
                            <td>kmmdfidr</td>
                            <td>56675gk434556456dfw34ufs</td>
                            <td ><a href="" class="btn btn-primary btn-custom waves-effect w-md waves-light m-b-5">UpdateGroup<a></td>
                            <td ><a href="" class="btn btn-success btn-custom waves-effect w-md waves-light m-b-5">UpdatePages</td>
                            <td >
                                <a href="" class="btn btn-info btn-custom waves-effect w-md waves-light m-b-5" 
                                style="background-color: #8b73d8; border: 1px solid #8b73d8;">UpdateFriends
                             </td>
                            <td>
                                <div class="form-check checkbox-slider-lg checkbox-slider--b-flat">
                                    <label>
                                        <input type="checkbox" checked=""><span></span>
                                    </label>
                                </div>
                            </td>
                            <td class="actions">
                                <a href="#" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                <a href="#" class="on-default remove-row" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
                                <a href="#" class="hidden on-editing save-row" data-toggle="tooltip" data-placement="top" title="" data-original-title="Save"><i class="fa fa-save"></i></a>
                                <a href="#" class="hidden on-editing cancel-row" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cancel"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        <tr class="gradeA">
                        <td><input type="checkbox"  name="" value="" checked style="width:20px;height:20px;"/></td>
                            <td>1921181701295125</td>
                            <td>goldman</td>
                            <td>ppppr</td>
                            <td>345kghfgk434556456dfw34ufs</td>
                            <td ><a href="" class="btn btn-primary btn-custom waves-effect w-md waves-light m-b-5">UpdateGroup<a></td>
                            <td ><a href="" class="btn btn-success btn-custom waves-effect w-md waves-light m-b-5">UpdatePages</td>
                            <td ><a href="" class="btn btn-info btn-custom waves-effect w-md waves-light m-b-5" 
                                style="background-color: #8b73d8; border: 1px solid #8b73d8;">UpdateFriends
                            </td>
                            <td>
                                <div class="form-check checkbox-slider-lg checkbox-slider--b-flat">
                                    <label>
                                        <input type="checkbox" checked=""><span></span>
                                    </label>
                                </div>
                            </td>
                            <td class="actions">
                                <a href="#" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                <a href="#" class="on-default remove-row" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
                                <a href="#" class="hidden on-editing save-row" data-toggle="tooltip" data-placement="top" title="" data-original-title="Save"><i class="fa fa-save"></i></a>
                                <a href="#" class="hidden on-editing cancel-row" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cancel"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- end: page -->
        </div> <!-- end Panel -->
    </div>
</div>
    
@endsection
@section('footerArea')
<script src="{{asset('plugins/magnific-popup/dist/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/tiny-editable/mindmup-editabletable.js')}}"></script>
<script src="{{asset('plugins/tiny-editable/numeric-input-example.js')}}"></script>
<script>
    $('#mainTable').editableTableWidget().numericInputExample().find('td:first').focus();
</script>
@endsection