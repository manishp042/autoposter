@extends('layouts.master')

@section('header')
<link rel="stylesheet" href="{{asset('plugins/magnific-popup/dist/magnific-popup.css')}}" />
<link rel="stylesheet" href="{{asset('plugins/jquery-datatables-editable/dataTables.bootstrap4.min.css')}}" />
<link href="{{asset('dark/assets/css/titatoggle-dist.css')}}" rel="stylesheet">
@endsection
@section('navbar')
<!-- Start content -->
<div class="content">
    <div class="container-fluid">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title"><span>Save Facebook Account</span></h4>
                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <!-- <li class="breadcrumb-item"><a href="#"></a></li> -->
                        <li class="breadcrumb-item active">Facebook Account</li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    @endsection
    @section('content')
    <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-md-1 text-xs-center">
                            <div class="form-group">
                                <!-- <label class="control-label m-r-5">Status</label> -->
                                <select id="demo-foo-filter-status" class="form-control input-sm btn btn-success" style="background:#00b19d;color:black !important">
                                    <option value="active" style="color:black !important">Active</option>
                                    <option value="" style="color:black !important">Deactive</option>
                                    <option value="active" style="color:black !important">Delete</option>
                                </select>
                            </div>
                        </div>
                    </div><hr><br>
                    <table class="table table-striped add-edit-table" id="datatable-editable">
                        <thead>
                        <tr>
                            <th>Check</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Type</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="gradeX">
                            <td><input type="checkbox"  name="" value="" style="width:20px;height:20px;"/></td>
                            <td>1921181701295125</td>
                            <td>Policeman</td>
                            <td>georigy</td>
                            <td>
                                <div class="form-check checkbox-slider-lg checkbox-slider--b-flat">
                                    <label><input type="checkbox" checked=""><span></span></label>
                                </div>
                            </td>
                            <td class="actions">
                                <a href="#" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                <a href="#" class="on-default remove-row" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
                                <a href="#" class="hidden on-editing save-row" data-toggle="tooltip" data-placement="top" title="" data-original-title="Save"><i class="fa fa-save"></i></a>
                                <a href="#" class="hidden on-editing cancel-row" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cancel"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        <tr class="gradeC">
                        <td><input type="checkbox"  name="" value="" style="width:20px;height:20px;"/></td>
                            <td>67619211817012951</td>
                            <td>kololiceman</td>
                            <td>kmmdfidr</td>
                            <td>
                                <div class="form-check checkbox-slider-lg checkbox-slider--b-flat">
                                    <label><input type="checkbox" checked=""><span></span></label>
                                </div>
                            </td>
                            <td class="actions">
                                <a href="#" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                <a href="#" class="on-default remove-row" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
                                <a href="#" class="hidden on-editing save-row" data-toggle="tooltip" data-placement="top" title="" data-original-title="Save"><i class="fa fa-save"></i></a>
                                <a href="#" class="hidden on-editing cancel-row" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cancel"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        <tr class="gradeA">
                            <td><input type="checkbox"  name="" value="" checked style="width:20px;height:20px;"/></td>
                            <td>1921181701295125</td>
                            <td>goldman</td>
                            <td>ppppr</td>
                            <td>
                                <div class="form-check checkbox-slider-lg checkbox-slider--b-flat">
                                    <label><input type="checkbox" checked=""><span></span></label>
                                </div>
                            </td>
                            <td class="actions">
                                <a href="#" class="on-default edit-row" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                <a href="#" class="on-default remove-row" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
                                <a href="#" class="hidden on-editing save-row" data-toggle="tooltip" data-placement="top" title="" data-original-title="Save"><i class="fa fa-save"></i></a>
                                <a href="#" class="hidden on-editing cancel-row" data-toggle="tooltip" data-placement="top" title="" data-original-title="Cancel"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- end: page -->
        </div> <!-- end Panel -->
    </div>
</div>        
    @endsection

    @section('footerArea')
        
        <!--Form Wizard-->
        <script src="{{asset('plugins/jquery.steps/js/jquery.steps.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript" ></script>

        <!--wizard initialization-->
        <script src="{{asset('dark/assets/pages/jquery.wizard-init.js')}}" type="text/javascript"></script>
    @endsection
