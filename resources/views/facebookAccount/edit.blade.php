@extends('layouts.master')
@section('header')
<!--Form Wizard-->
<link rel="stylesheet" type="text/css" href="{{asset('plugins/jquery.steps/css/jquery.steps.css')}}" />
@endsection
@section('navbar')
<!-- Start content -->
<div class="content">
    <div class="container-fluid">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h3 class="m-t-0 header-title"><b>Update Faceboook Account</b></h3>
                    <!-- <h4 class="page-title"><span>Update Faceboook Account</span></h4> -->
                    <ol class="breadcrumb float-right">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <!-- <li class="breadcrumb-item"><a href="#"></a></li> -->
                        <li class="breadcrumb-item active">Facebook Account</li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    @endsection
    @section('content')
        <!-- Basic Form Wizard -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    <h5 class="m-t-0 header-title"><b>Step1 -Get Access Token</b></h5>
                    <p class="text-muted m-b-20 font-13"></p>
                    <form id="basic-form" action="#">
                        <div>
                            <h3>Facebook Username</h3>
                            <section>
                                <div class="form-group clearfix">
                                    <label class="control-label col-md-3" for="userName">User name *</label>
                                    <div class="col-lg-12">
                                        <input class="form-control required" id="email" name="email" type="text">
                                    </div>
                                </div>
                            </section>

                            <h3>Facebook Password</h3>
                            <section>
                                <div class="form-group clearfix">
                                    <label class="control-label col-md-3" for="userName">Password*</label>
                                    <div class="col-lg-12">
                                        <input class="form-control required" id="password" name="password" type="text">
                                    </div>
                                </div>
                            </section>
                            
                            <h3>Facebook App</h3>
                            <section>
                                <div class="form-group clearfix">
                                    <label class="control-label" for="name"> First name *</label>
                                    <div class="col-lg-12">
                                        <select class="form-control">
                                            <option>Facebook For Android</option>
                                            <option>Facebook For Iphone</option>
                                        </select>
                                    </div>
                                </div>
                            </section>
                            <!-- <section>
                                <div class="form-group clearfix">
                                    <div class="col-lg-12">
                                        <ul class="list-unstyled w-list">
                                            <li><b>First Name :</b> Jonathan </li>
                                            <li><b>Last Name :</b> Smith </li>
                                            <li><b>Emial:</b> jonathan@smith.com</li>
                                            <li><b>Address:</b> 123 Your City, Cityname. </li>
                                        </ul>
                                    </div>
                                </div>
                            </section> -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- End row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    <h5 class="m-t-0 header-title"><b>Step2 -Add Access Token</b></h5>
                    <p class="text-muted m-b-20 font-13"></p>
                    <form id="second-form" action="#">
                        <div class="form-group clearfix">
                            <label class="control-label col-md-3" for="userName">Access Token</label>
                            <div class="col-lg-12">
                                        <!-- <div class="col-10"> -->
                                <textarea class="form-control" rows="5"></textarea>
                                    <!-- </div> -->
                            </div>
                            <button type="button" class="btn btn-success btn-custom waves-effect w-md waves-light m-b-5 m-t-5" style="margin-left:10px;">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    @endsection

    @section('footerArea')
        
        <!--Form Wizard-->
        <script src="{{asset('plugins/jquery.steps/js/jquery.steps.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript" ></script>

        <!--wizard initialization-->
        <script src="{{asset('dark/assets/pages/jquery.wizard-init.js')}}" type="text/javascript"></script>
    @endsection
