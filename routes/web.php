<?php

Route::get('/', function () {
    return view('welcome');
});
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; //Return anything
});

Auth::routes();
Route::get('auth/facebook', 'Auth\FacebookController@redirectToFacebook');
Route::get('auth/facebook/callback', 'Auth\FacebookController@handleFacebookCallback');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/facebookAccount', 'HomeController@facebookAccount')->name('facebookAccount');
Route::get('/fbAccount/edit', 'HomeController@fBedit')->name('facebookAccount.edit');
Route::get('/fbAccount/save', 'HomeController@save')->name('facebookAccount.save');
Route::get('/autoPost/scheduled', 'HomeController@scheduledPost')->name('autoPost.scheduledPost');

Route::get('/autoPost/addnew', 'HomeController@addnew')->name('autoPost.addnew');

Route::get('/maps/googleMap', 'HomeController@googleMap')->name('maps.googleMap');

Route::post('/form/text', 'HomeController@text')->name('text');