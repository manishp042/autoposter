<?php
namespace App\Http\Controllers\Auth;
use App\User;
use App\Http\Controllers\Controller;
use Socialite;
use Exception;
use Auth;

class FacebookController extends Controller
{

    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }
    public function handleFacebookCallback()
    {
        try {
            $user = Socialite::driver('facebook')->user();
            // $create['username'] = $user->getSurname();
            $create['name'] = $user->getName();
            $create['email'] = $user->getEmail();
            $create['facebook_id'] = $user->getId();
            $create['token'] = $user->token;
            $create['avatar'] = $user->getAvatar();
            $userModel = new User;
            $createdUser = $userModel->addNew($create);
            $fbUser=Auth::loginUsingId($createdUser->id);
            return redirect()->route('facebookAccount');
        } 
        catch (Exception $e) 
        {
            //\Log::error($e);
            return redirect('login');

        }
    }
}