<?php

namespace App\Http\Controllers;
use App\User;
use Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.index');
    }
    public function facebookAccount()
    {
        
        return view('facebookAccount.index');
    }

    public function fBedit()
    {
        $fbUser=User::get();
        // dd($fbUser);
        return view('facebookAccount.edit',compact('fbUser'));
    }

    public function save()
    {
        return view('facebookAccount.save');
    }
    public function scheduledPost()
    {
        return view('autoPost.scheduledPost');
    }
    
    public function addNew()
    {
        return view('autoPost.addNew');
    }

    public function googleMap()
    {
        return view('maps.googleMap');
    }

     public function text(Request $request)
    {
        $title = $request->input('title');
        $content = $request->input('content');
        return view('maps.googleMap');
    }

}
